﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class login : MonoBehaviour
{
    public InputField userID, password;
    public Text txtMessage;
    public static string userName;
    private bool isValidated = false;

    private void OnEnable()
    {
        isValidated = false;
    }

    public void  sceneChange()
    {
        if(isValidated)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Test Scene");
    }

    public void ValidateInputField()
    {
        if (userID.text == string.Empty)
            txtMessage.text = "Please enter user ID";
        else if (password.text == string.Empty)
            txtMessage.text = "Please enter Password";
        else if (userID.text == string.Empty && password.text == string.Empty)
            txtMessage.text = "Please enter userid and Password";
        else if(userID.text != string.Empty && password.text != string.Empty)
            isValidated = true;

        if (userID.text !=string.Empty)
            userName = userID.text;
    }
}
