﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootingHandler : MonoBehaviour {
 
   

    public int bullets = 30;
    public float fireRate = 0.3f;
    float frate;
    Animator anim;
    public Text txtFireBullet;

    bool fire;

    void Start()
    {
        anim = GetComponent<Animator>();
        txtFireBullet.text = "Bullets : " + bullets.ToString() + "  Player Name = " + login.userName;
    }

	void Update () 
    {
        if (Input.GetMouseButton(0))
        {
            fire = true; 
        }

        if (fire)
        {
            frate += Time.deltaTime;

            if (frate > fireRate)
            {
                ParticleSystem[] parts = GetComponentsInChildren<ParticleSystem>();

                if (bullets > 0)
                {
                    foreach (ParticleSystem ps in parts)
                    {
                        ps.Emit(1);
                    }

                    txtFireBullet.text = "Bullets : " + bullets.ToString() + "  Player Name = " + login.userName;
                    bullets--;
                }
                else
                {
                    anim.SetBool("Reload", true);
                    StartCoroutine("CloseReload");
                }
                frate = 0;
                fire = false;
            }
        }
	}

    IEnumerator CloseReload()
    {
        yield return new WaitForSeconds(1);
        anim.SetBool("Reload", false);
        yield return new WaitForSeconds(0.5f);
        bullets = 30;
    }
}
